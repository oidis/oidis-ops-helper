#!/usr/bin/env node
/* ********************************************************************************************************* *
 *
 * Copyright 2024 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

import * as fs from "node:fs";
import { GitHelper } from "./src/GitHelper.mjs";
import { parseArgs } from "node:util";
import process from "node:process";

const args = parseArgs({
    strict: true,
    allowPositionals: true,
    options: {
        tag: {
            type: "boolean",
            default: false
        },
        update: {
            type: "boolean",
            default: false
        },
        "get-version": {
            type: "boolean",
            default: false
        },
        path: {
            type: "string",
            default: process.cwd()
        },
        username: {
            type: "string"
        },
        password: {
            type: "string"
        },
        version: {
            type: "string"
        },
        branch: {
            type: "string"
        },
        help: {
            type: "boolean",
            default: false
        }
    }
});

if (args.values.help) {
    console.log(`
Oidis ops-helper usage:
    --tag        Create and push new tag
    --update     Update project version and push changes
    --branch=    Checkout repository to specified branch
    --path=      Specify path to use instead of process.cwd()
    --username=  User with access to repository
    --password=  Password or token (preferred) with access to repository
    --version=   Explicit version to update project to
`);
    process.exit(0);
}

const helper = new GitHelper();
helper.WorkingDirectory = args.values.path;
helper.Credentials = {};

let path = helper.WorkingDirectory + "/private.json";
if (fs.existsSync(path)) {
    try {
        const data = JSON.parse(fs.readFileSync(path).toString());
        if (data?.credentials?.username) {
            helper.Credentials.username = data.credentials.username;
        }
        if (data?.credentials?.password) {
            helper.Credentials.password = data.credentials.password;
        }
    } catch (e) {
        console.error("Failed to parse private.json: " + e.message);
    }
}

helper.Credentials.username = args.values.username || helper.Credentials.username;
helper.Credentials.password = args.values.password || helper.Credentials.password;

let promise;
if (args.values["get-version"]) {
    promise = helper.getProjectVersion().then((version)=>{
        console.log(version);
    });
} else if (args.values.branch) {
    promise = helper.Checkout(args.values.branch);
} else if (args.values.tag) {
    promise = helper.PushTag();
} else if (args.values.update) {
    promise = helper.UpdateVersion(args.values.version);
} else {
    promise = new Promise(($resolve) => {
        console.error("No actions selected");
        $resolve();
    });
}

promise.then(() => {
    process.exit(0);
}).catch((error) => {
    console.error(error);
    process.exit(1);
});
