# oidis-ops-helper

> Utilities to simplify various OPS tasks and general automation.

This utility includes the GitHelper class, which assists with the automated maintenance and updating of project versions. It is specifically designed to be compatible with the [Oidis framework](https://gitlab.com/oidis) project configuration and [NPM modules](https://docs.npmjs.com/creating-a-package-json-file). The supported configuration files include:
* package.config.jsonp for [Oidis framework](https://gitlab.com/oidis)
* package.json for [NPM module](https://docs.npmjs.com/creating-a-package-json-file)

## Installation
To install oidis-ops-helper, you need to have Node.js and npm installed. Once you have them set up, you can install oidis-ops-helper globally using the following command:
```shell
npm install -g @oidis/ops-helper
```

## Usage
After installation, you can access the oidis-ops-helper CLI tool. To see the list of available commands and options, you can use the --help flag:
```shell
oidis-ops-helper --help
```
This command will display detailed information on how to use the tool, including various commands and their descriptions.

## Uninstallation
If you need to remove oidis-ops-helper from your system, you can do so by running:
```shell
npm uninstall -g @oidis/ops-helper
```
This will uninstall the tool and remove it from your global npm modules.

## License

This software is owned or controlled by Oidis.
The use of this software is governed by the BSD-3-Clause Licence distributed with this material.

See the `LICENSE.txt` file for more details.

---

Copyright 2024 [Oidis](https://www.oidis.io/)
