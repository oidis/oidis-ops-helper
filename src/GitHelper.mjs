/* ********************************************************************************************************* *
 *
 * Copyright 2024 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
import * as crypto from "node:crypto";
import * as fs from "node:fs";
import * as os from "node:os";
import { debuglog } from "node:util";
import { exec } from "node:child_process";

const debug = debuglog("GitHelper");

export class GitHelper {
    WorkingDirectory = "";
    DryRun = false;
    Credentials = {};

    /**
     * @param cmd
     * @param [args]
     * @param [cwd]
     * @returns {Promise<string[]>} [0] - stdout, [1] - stderr
     */
    async execute(cmd, args, cwd) {
        if (!Array.isArray(cmd)) {
            cmd = [cmd];
        }
        args = args || [];
        cwd = cwd || this.WorkingDirectory;
        return new Promise(($resolve, $reject) => {
            const fullCmd = cmd.concat(args).join(" ");
            debug(fullCmd);
            exec(fullCmd, {windowsHide: true, cwd}, (err, stdOut, stdErr) => {
                if (err) {
                    $reject(err);
                } else {
                    debug("stdout: " + stdOut + "\nstderr: " + stdErr);
                    $resolve([stdOut, stdErr]);
                }
            });
        });
    }

    async runGit(args, cwd) {
        await this.prepareCred();
        const tearDown = () => {
            if (fs.existsSync(this.credPath)) {
                fs.readFileSync(this.credPath);
            }
        };
        const cmd = `git -c core.askpass=${this.credPath} -c credential.helper=`;
        if (!Array.isArray(args)) {
            args = [args];
        }
        let result;
        try {
            result = (await this.execute(cmd, args, cwd))[0];
        } finally {
            tearDown();
        }
        return result;
    }

    async prepareCred() {
        if (!this.Credentials.username) {
            throw new Error("Missing username");
        }
        if (!this.Credentials.password) {
            throw new Error("Missing user password");
        }
        this.credPath = os.tmpdir() + "/" + crypto.createHash("sha1")
            .update((new Date()).valueOf().toString() + Math.random().toString()).digest("hex");
        const content = `
        #!/bin/bash
        if [ "$1" = "Username for"* ]; then
            echo "${this.Credentials.username}"
        else
            echo "${this.Credentials.password}"
        fi
        `;
        fs.writeFileSync(this.credPath, content, {mode: 0o755});
    }

    parseVersion = (version) => {
        const [main, suffix] = version.split(/[-_]/);
        const [major, minor, patch] = main.split('.').map(Number);
        return {major, minor, patch, suffix: suffix || ''};
    };

    compareVersion = (a, b) => {
        const verA = this.parseVersion(a);
        const verB = this.parseVersion(b);
        if (verA.major !== verB.major) {
            return verA.major - verB.major;
        }
        if (verA.minor !== verB.minor) {
            return verA.minor - verB.minor;
        }
        if (verA.patch !== verB.patch) {
            return verA.patch - verB.patch;
        }
        return verA.suffix.localeCompare(verB.suffix);
    };

    async getTags(origin = false) {
        let cmd = "tag --list";
        if (origin) {
            cmd = "ls-remote --tags origin";
        }
        const output = await this.runGit(cmd, this.WorkingDirectory);
        return output.split("\n")
            .filter((item) => item)
            .map((item) => {
                let val = item.replace(/\r/g, "").replace(/\^.*/g, '').trim();
                if (origin) {
                    const match = val.match(/refs\/tags\/(.+)/);
                    val = match ? match[1] : null;
                }
                return val;
            })
            .filter((item, index, self) => self.indexOf(item) === index)
            .sort(this.compareVersion).reverse();
    }

    /**
     * @returns {Promise<string>}
     */
    async getProjectVersion() {
        let path;
        let retVal = "N/A";
        if (fs.existsSync(path = this.WorkingDirectory + "/package.conf.jsonp")) {
            try {
                retVal = (await this.execute(["oidis --info='version'"], [], this.WorkingDirectory))[0];
            } catch (e) {
                debug("oidis builder not found or failed: " + e.message);
                const matches = fs.readFileSync(path).toString().match(/version:\s*["'](\d+\.\d+\.\d+)["']/);
                if (matches.length >= 1) {
                    retVal = matches[1];
                }
            }
        } else if (fs.existsSync(path = this.WorkingDirectory + "/package.json")) {
            const matches = fs.readFileSync(path).toString().match(/["']version["']\s*:\s*["'](\d+\.\d+\.\d+)["']/);
            if (matches.length >= 1) {
                retVal = matches[1];
            }
        } else {
            throw new Error("Selected working directory is not compatible with Oidis or Node.js module project structure.");
        }
        retVal = retVal.trim();
        if (!retVal) {
            throw new Error("Project version not found or project is corrupted. " +
                "Check if package.conf.jsonp or package.json contains version property.");
        }
        return retVal;
    }

    async updateProjectVersion(version) {
        let path;
        if (fs.existsSync(path = this.WorkingDirectory + "/package.conf.jsonp")) {
            fs.writeFileSync(path, fs.readFileSync(path).toString().replace(/version:\s*["']\d+\.\d+\.\d+["']/, `version: "${version}"`));
            await this.execute(`git add package.conf.jsonp`);
            await this.execute(`git commit --no-verify -m 'CleanUp: automatic version update: ${version}'`);
            let branch = (await this.execute("git branch --show-current"))?.at(0).trim();
            if (!branch) {
                throw new Error("Failed to gather branch to push in");
            }
            if (!this.DryRun) {
                return await this.runGit(`push origin ${branch}`);
            }
        } else if (fs.existsSync(path = this.WorkingDirectory + "/package.json")) {
            try {
                const data = JSON.parse(fs.readFileSync(path).toString());
                data.version = version;
                fs.writeFileSync(path, JSON.stringify(data, null, 2));
            } catch (e) {
                throw new Error(`Failed to update version in ${path}: ${e.message}`);
            }
            await this.execute(`git add package.json`);
            await this.execute(`git commit --no-verify -m 'CleanUp: automatic version update: ${version}'`);
            let branch = (await this.execute("git branch --show-current"))?.at(0).trim();
            if (!branch) {
                throw new Error("Failed to gather branch to push in");
            }
            if (!this.DryRun) {
                return await this.runGit(`push origin ${branch}`);
            }
        } else {
            throw new Error("Selected working directory is not compatible with Oidis or Node.js module project structure.");
        }
    }

    async createTag(tag, message = "") {
        if (!this.DryRun) {
            if (message.length > 0) {
                return await this.runGit(`tag -a ${tag} -m "${message}"`);
            }
            return await this.runGit(`tag ${tag}`);
        }
    }

    async pushTag(tag) {
        if (!this.DryRun) {
            return await this.runGit(`push origin ${tag}`);
        }
    }

    async cleanUp() {
        console.log("Cleaning up...");
        if (!this.DryRun) {
            await this.execute("git reset --hard");
            await this.execute("git clean -fd");
        }
    }

    async PushTag() {
        console.log("Pushing tag...");
        await this.cleanUp();
        const version = await this.getProjectVersion();
        const tags = await this.getTags(true);
        const localTags = await this.getTags();

        if (tags.includes(version)) {
            throw new Error(`Repository already contains tag ${version}`);
        }
        if (!localTags.includes(version)) {
            await this.createTag(version, "oidis: automated tag update");
        } else {
            console.log(`Local repository already contains tag ${version}`);
        }
        console.log(await this.pushTag(version));
        console.log("Tag pushed to origin.");
    }

    getCurrentDate() {
        return new Date();
    }

    increaseVersion(version) {
        const getYear = (date, tolerance = 15) => {
            let startOfYear = new Date(date.getFullYear(), 0, 1);
            let endOfYear = new Date(date.getFullYear(), 11, 31);
            let diffToEnd = (endOfYear - date) / (1000 * 60 * 60 * 24);
            let diffToStart = (date - startOfYear) / (1000 * 60 * 60 * 24);

            if (diffToEnd >= -tolerance && diffToEnd <= tolerance) {
                return endOfYear.getFullYear() + 1;
            } else if (diffToStart >= -tolerance && diffToStart <= tolerance) {
                return startOfYear.getFullYear();
            } else {
                return date.getFullYear();
            }
        };

        const getQuarter = (date, tolerance = 15) => {
            let quarters = [0, 3, 6, 9];
            let startOfYear = new Date(date.getFullYear(), 0, 1);
            let diffToStart = (date - startOfYear) / (1000 * 60 * 60 * 24);

            for (let i = 0; i < quarters.length; i++) {
                let endOfQuarter = new Date(date.getFullYear(), quarters[i] + 3, 0);
                let diffToEnd = (endOfQuarter - date) / (1000 * 60 * 60 * 24);
                if (diffToEnd >= -tolerance && diffToEnd <= tolerance) {
                    return i + 1;
                } else if (diffToStart >= -tolerance && diffToStart <= tolerance) {
                    return i;
                }
            }

            return Math.floor(date.getMonth() / 3);
        };

        const parts = this.parseVersion(version);
        const date = this.getCurrentDate();

        const year = getYear(date);
        let quarter = getQuarter(date);

        if (year <= parts.major) {
            if (quarter <= parts.minor) {
                parts.patch++;
            } else {
                parts.minor = quarter;
                parts.patch = 0;
            }
        } else {
            parts.major = year;
            parts.minor = 0;
            parts.patch = 0;
        }
        return `${parts.major}.${parts.minor}.${parts.patch}` + (parts.suffix ? "-" + parts.suffix : "");
    }

    async UpdateVersion(version = "") {
        const currentVersion = await this.getProjectVersion();
        const versions = await this.getTags(true);
        console.log("Current project version: " + currentVersion);
        if (version) {
            console.log(`Update to explicit version: ${version}`);
            if (!(this.compareVersion(version, currentVersion) > 0)) {
                throw new Error("Specified version needs to be greater than current version.");
            }
        } else {
            if (versions.length > 0 && (this.compareVersion(currentVersion, versions[0]) > 0)) {
                console.log(`Project version ${currentVersion} is already greater than latest tag. Update skipped.`)
                return currentVersion;
            }
            version = currentVersion;
            version = this.increaseVersion(version);
            console.log(`Update to auto-deduced version: ${version}`);
        }

        if (versions.length > 0 && !(this.compareVersion(version, versions[0]) > 0)) {
            version = versions[0];
            debug(`Resolved version (${version}) is lower than already tagged, providing another increase...`);
            version = this.increaseVersion(version);
        }
        await this.updateProjectVersion(version);
        console.log("Project version updated successfully.");
        return version;
    }

    async Checkout(branch) {
        await this.cleanUp();
        await this.runGit(`fetch origin refs/heads/${branch}:refs/remotes/origin/${branch} --depth=1`);
        try {
            await this.execute(`git rev-parse --verify ${branch}`);
            await this.runGit(`checkout ${branch}`);
        } catch (e) {
            debug(`branch existence check: ${e.message}`);
            await this.runGit(`checkout -b ${branch} origin/${branch}`);
        }
        await this.runGit(`pull origin ${branch}`);
    }
}
