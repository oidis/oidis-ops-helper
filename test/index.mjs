/* ********************************************************************************************************* *
 *
 * Copyright 2024 Oidis
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
import { describe, it } from "mocha/lib/mocha.js";
import { GitHelper } from "../src/GitHelper.mjs";
import assert from "assert";

const helper = new GitHelper();
let execRet = () => {
    return "";
};

helper.runGit = async () => {
    return execRet();
};
helper.execute = () => {
    // mock
};
helper.updateProjectVersion = () => {
    // mock
};
helper.PushTag = () => {
    // mock
};

async function handleError(block) {
    let error;
    try {
        if (!!block && typeof block.then === 'function') {
            await block;
        } else {
            await block();
        }
    } catch (e) {
        error = e;
    }
    return error;
}

console.log = () => {
    // dummy
};

describe("git-helper-test", function () {
    it("getTags", async () => {
        execRet = () => {
            return "" +
                "1.1.2-connector\n" +
                "1.1.3\n" +
                "1.1.4\n" +
                "2024.0.0\n" +
                "2024.0.1\n";
        };
        const tags = await helper.getTags();
        assert.deepEqual(tags, [
            "2024.0.1",
            "2024.0.0",
            "1.1.4",
            "1.1.3",
            "1.1.2-connector"]);
    });

    it("getTags_origin", async () => {
        execRet = () => {
            return "" +
                "ea0d8c12c83179ca3f1aca59fc9fc2094c1c7356        refs/tags/2.0\n" +
                "210c454023559451c76accd2224cb9396aeac14c        refs/tags/2.1\n" +
                "ccece9975ede50c5c50acc0a3f4e2ea7cbd99d9f        refs/tags/1.0.0\n" +
                "9af4630b54caf88ffac9284ac467ef51c1ed4885        refs/tags/3.2.0\n" +
                "603a55570763e2e7b2d1eacc02b8ae44091fce60        refs/tags/2024.0.0\n" +
                "603a55570763e2e7b2d1eacc02b8ae44091fce60        refs/tags/2024.0.0^{}\n" +
                "ff43c74fe82f0b60978e3acc7ae31922bb85e86c        refs/tags/2024.0.1\n";
        };
        const tags = await helper.getTags(true);
        assert.deepEqual(tags, ["2024.0.1", "2024.0.0", "3.2.0", "2.1", "2.0", "1.0.0"]);
    });

    describe("increaseVersion", () => {
        const filter = [];
        const data = [
            ["2023-12-10", "2023.2.19", "2023.3.0"],
            ["2023-12-10", "2023.3.19", "2023.3.20"],
            ["2023-12-20", "2023.2.19", "2024.0.0"],
            ["2023-12-20", "2023.3.1", "2024.0.0"],

            ["2023-12-31", "2023.3.29", "2024.0.0"],
            ["2024-01-01", "2023.0.0", "2024.0.0"],
            ["2024-01-01", "2024.0.0", "2024.0.1"],
            ["2024-01-15", "2024.0.3", "2024.0.4"],

            ["2024-03-10", "2024.0.19", "2024.0.20"],
            ["2024-03-20", "2024.0.19", "2024.1.0"],
            ["2024-03-31", "2024.0.29", "2024.1.0"],
            ["2024-04-01", "2024.0.0", "2024.1.0"],
            ["2024-04-01", "2024.1.0", "2024.1.1"],

            ["2024-06-10", "2024.1.19", "2024.1.20"],
            ["2024-06-20", "2024.1.19", "2024.2.0"],
            ["2024-06-30", "2024.2.0", "2024.2.1"],
            ["2024-07-01", "2024.2.0", "2024.2.1"],

            ["2024-09-10", "2024.2.19", "2024.2.20"],
            ["2024-09-30", "2024.2.29", "2024.3.0"],
            ["2024-10-01", "2024.3.0", "2024.3.1"],

            ["2024-12-10", "2024.3.19", "2024.3.20"],
            ["2024-12-20", "2024.3.19", "2025.0.0"],
            ["2024-12-31", "2024.3.29", "2025.0.0"],
            ["2025-01-01", "2025.0.1", "2025.0.2"],

            ["2025-02-01", "2025.2.1", "2025.2.2"],
            ["2025-01-01", "2026.2.1", "2026.2.2"]
        ];
        for (const item of data) {
            if ((filter.length > 0 && filter.includes(data.indexOf(item)))|| filter.length===0) {
                it(`[${data.indexOf(item)}] - ${item}`, () => {
                    helper.getCurrentDate = () => new Date(item[0]);
                    assert.deepEqual(helper.increaseVersion(item[1]), item[2]);
                });
            }
        }
    });

    it("UpdateVersion_explicit", async () => {
        helper.getProjectVersion = async () => "2024.1.0";
        assert.deepEqual(
            (await handleError(helper.UpdateVersion("2024.1.0"))).message,
            "Specified version needs to be greater than current version.");

        helper.getProjectVersion = async () => "2024.2.0";
        assert.deepEqual(
            (await handleError(helper.UpdateVersion("2024.1.0"))).message,
            "Specified version needs to be greater than current version.");

        helper.getProjectVersion = async () => "2024.1.0";
        assert.deepEqual(
            (await handleError(helper.UpdateVersion("2024.2.0"))),
            undefined);

        helper.getProjectVersion = async () => "2024.1.0";
        assert.deepEqual(await helper.UpdateVersion("2025.1.1"), "2025.1.1");
    });

    it("UpdateVersion_automatic", async () => {
        execRet = () => {
            return "" +
                "ff43c74fe82f0b60978e3acc7ae31922bb85e86c        refs/tags/0.0.0\n";
        };
        helper.getCurrentDate = () => new Date("2024-01-05");
        helper.getProjectVersion = async () => "2024.1.0";
        assert.deepEqual(await helper.UpdateVersion(), "2024.1.1");

        helper.getCurrentDate = () => new Date("2024-01-05");
        helper.getProjectVersion = async () => "2024.0.0";
        assert.deepEqual(await helper.UpdateVersion(), "2024.0.1");

        helper.getCurrentDate = () => new Date("2024-03-25");
        helper.getProjectVersion = async () => "2024.0.1";
        assert.deepEqual(await helper.UpdateVersion(), "2024.1.0");

        helper.getCurrentDate = () => new Date("2024-12-25");
        helper.getProjectVersion = async () => "2024.3.1";
        assert.deepEqual(await helper.UpdateVersion(), "2025.0.0");

        helper.getCurrentDate = () => new Date("2024-12-15");
        helper.getProjectVersion = async () => "2024.3.1";
        assert.deepEqual(await helper.UpdateVersion(), "2024.3.2");

        helper.getCurrentDate = () => new Date("2024-7-05");
        helper.getProjectVersion = async () => "2024.1.4";
        assert.deepEqual(await helper.UpdateVersion(), "2024.2.0");

        execRet = () => {
            return "" +
                "ff43c74fe82f0b60978e3acc7ae31922bb85e86c        refs/tags/2024.2.0\n";
        };
        helper.getCurrentDate = () => new Date("2024-08-05");
        helper.getProjectVersion = async () => "2024.1.4";
        assert.deepEqual(await helper.UpdateVersion(), "2024.2.1");

        execRet = () => {
            return "" +
                "ff43c74fe82f0b60978e3acc7ae31922bb85e86c        refs/tags/2024.0.9\n"+
                "ff43c74fe82f0b60978e3acc7ae31922bb85e86c        refs/tags/2024.1.0";
        };
        helper.getCurrentDate = () => new Date("2024-06-05");
        helper.getProjectVersion = async () => "2023.0.4";
        assert.deepEqual(await helper.UpdateVersion(), "2024.1.1");

        execRet = () => {
            return "" +
                "ff43c74fe82f0b60978e3acc7ae31922bb85e86c        refs/tags/2024.1.12\n"+
                "ff43c74fe82f0b60978e3acc7ae31922bb85e86c        refs/tags/2024.1.13";
        };
        helper.getCurrentDate = () => new Date("2024-06-05");
        helper.getProjectVersion = async () => "2024.1.13";
        assert.deepEqual(await helper.UpdateVersion(), "2024.1.14");
    });
});
